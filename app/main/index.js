// This is free and unencumbered software released into the public domain.
// See LICENSE for details
const electron = require('electron')
const {app, BrowserWindow, dialog, Menu, protocol, ipcMain} = electron;
const log = require('electron-log');
const {autoUpdater} = require("electron-updater");
const path = require('path')
const url = require('url')
const isDev = require('electron-is-dev');

//-------------------------------------------------------------------
// Logging
//
// THIS SECTION IS NOT REQUIRED
//
// This logging setup is not required for auto-updates to work,
// but it sure makes debugging easier :)
//-------------------------------------------------------------------
autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';
log.info('App starting...');

//-------------------------------------------------------------------
// Define the menu
//
// THIS SECTION IS NOT REQUIRED
//-------------------------------------------------------------------
menuTemplate = 
[
  {
    label: 'File',
    submenu:
      [
        {
          label: 'Register User',
          click: () => {
            openRegisterUser()
          }
        },
        {
          label: 'Exit',
          accelerator: 'CmdOrCtrl+W',
          role: 'close'
        }]
  },
  {
    label: 'View',
    submenu:
      [
        {
          label: 'Toggle Full Screen',
          accelerator: (function ()
          {
            if(process.platform === 'darwin')
            {
              return 'Ctrl+Command+F'
            }
            else
            {
              return 'F11'
            }
          })(),
          click: function (item, focusedWindow)
          {
            if (focusedWindow)
            {
              focusedWindow.setFullScreen(!focusedWindow.isFullScreen())
            }
          }
        }]
  },
  {
    label: 'About',
    submenu:
      [
        {
          label: 'About',
          click: function (item, focusedWindow)
          {
            if (focusedWindow)
            {
              const options = {
                type: 'info',
                title: 'About HIVOS 4S@Scale Data Entry Tool',
                buttons: ['Ok'],
                message: '2017 Hivos International. Application built by Glenwell Limited. App Version: ' + app.getVersion()
              }
              electron.dialog.showMessageBox(focusedWindow, options, function () {})
            }
          }
        }]
  }]


//-------------------------------------------------------------------
// Open a window that displays the version
//
// THIS SECTION IS NOT REQUIRED
//
// This isn't required for auto-updates to work, but it's easier
// for the app to show a window than to have to click "About" to see
// that updates are working.
//-------------------------------------------------------------------
let win;

function sendStatusToWindow(text) {
  log.info(text);
  win.webContents.send('message', text);
}
function createDefaultWindow() {
  win = new BrowserWindow({
    width: 876,
    minWidth: 876,
    height: 660,
    minHeight: 660,
    resizable: true,
    icon: path.join(__dirname, '../renderer/assets/img/64x64.png')
  });

  // Set up the menu
  var menu = Menu.buildFromTemplate(menuTemplate)
  win.setMenu(menu)

  // win.webContents.openDevTools();

  win.on('closed', () => {
    win = null;
  });
  win.loadURL(`file://${__dirname}/../renderer/app-login.html#v${app.getVersion()}`);
  return win;
}

// Opens the register user window
function openRegisterUser() {

  let aboutWindow = new BrowserWindow({
    parent: win,
    modal: true,
    resizable: false,
    show: false,
    width: 600,
    height: 500,
    icon: path.join(__dirname, '../renderer/assets/img/64x64.png')
  })
  aboutWindow.loadURL(url.format({
    pathname: path.join(__dirname, '../renderer/app-register-user.html'),
    protocol: 'file:',
    slashes: true
  }))
  aboutWindow.setMenu(null)
  aboutWindow.once('ready-to-show', () => {
    aboutWindow.show()
  })
}

// function createDatabase() {
//   const fs = require('fs-extra')
//   const path = require('path')
//   const userDataPath = app.getPath('userData');
//   const file = path.join(__dirname, '/databases');

//   //Check if the database exists. If not, create the files.
//   fs.pathExists(file, (err, exists) => {
//     if(!exists){
//       const activeUser = path.join(userDataPath, '/databases/ActiveUser.db');
//       const farmerData = path.join(userDataPath, '/databases/FarmerData.db');
//       const userData = path.join(userDataPath, '/databases/UserData.db');

//       fs.outputFileSync(activeUser, '');
//       fs.outputFileSync(farmerData, '');
//       fs.outputFileSync(userData, '');
//       log.info(userDataPath);
//     }
//   })
// }

autoUpdater.on('checking-for-update', () => {
  // sendStatusToWindow('Checking for update...');
  log.info('Checking for update...');
})

autoUpdater.on('update-available', (info) => {
  // sendStatusToWindow('Downloading update.');
  log.info('Update available.');
})

autoUpdater.on('update-not-available', (info) => {
  log.info('Update not available.');
})

autoUpdater.on('error', (err) => {
  sendStatusToWindow('Error in auto-updater. ' + err);
  log.info('Error in auto-updater. ' + err);
})

// autoUpdater.on('download-progress', (progressObj) => {
//   let log_message = "Download speed: " + progressObj.bytesPerSecond;
//   log_message = log_message + ' - Downloaded ' + progressObj.percent + '%';
//   log_message = log_message + ' (' + progressObj.transferred + "/" + progressObj.total + ')';
//   sendStatusToWindow(log_message);
//   log.info(log_message);
// })

autoUpdater.on('update-downloaded', event => {
    // Ask user to update the app
    dialog.showMessageBox({
      type: 'question',
      buttons: ['Install Now', 'Install Later'],
      defaultId: 0,
      message: `A new update ${event.version} has been downloaded`,
      detail: 'It will be installed the next time you restart the application'
    }, response => {
      if (response === 0) {
        setTimeout(() => {
          autoUpdater.quitAndInstall();
          // force app to quit. This is just a workaround, ideally autoUpdater.quitAndInstall() should relaunch the app.
          app.quit();
        }, 1000);
      }
    });
  });

app.on('ready', function() {
  //Create directories if they do not exist
  // createDatabase();
  //Create window
  createDefaultWindow();
});

// Quit when all windows are closed
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

// Reopen the app on macOS
app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

//-------------------------------------------------------------------
// Auto updates
//
// This will immediately download an update, then install when the
// app quits.
//-------------------------------------------------------------------
app.on('ready', function()  {
  if (isDev) {
    log.info('Dev environment detected');
    return;
  }else{
    autoUpdater.checkForUpdates();
  }
});