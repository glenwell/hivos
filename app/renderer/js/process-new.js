//Script to register user
$(document).ready(function()
{
	
	//Variable to hold the result of the register request
	var resultregister;
	var path= 'https://hivos.glenwell.com/admin-portal/mobile-processes';
	var options = { 
			url: path,
			beforeSubmit:  beforeregister,  // pre-submit callback 
			success:       afterregister,  // post-submit callback
			cache : false,
			resetForm: true        // reset the form after successful submit 
		}; 
		
	$('#registerform').submit(function()
	{ 
		$(this).ajaxSubmit(options);  			
		// always return false to prevent standard browser submit and page navigation 
		return false; 
	}); 
	
	//Function after register response received
	function afterregister(resultregister)
	{
		if(resultregister==101)
		{
			alert('Your Details Have Been Sent! Await Confirmation From Admin');
			return false;
		}
		else if(resultregister==102)
		{
			alert('Details Not Saved. Contact Admin');
			return false;
		}
		else if(resultregister==103)
		{
			alert('You are not registered on our system.');
			return false;
		}
		else
		{
			alert(resultregister);
			return false;
		}
	}

	//before register
	function beforeregister()
	{
		var online = navigator.onLine;
		
	  	var myresult = $('#registerform').parsley().validate();
	  	if(!myresult)
	  	{
	  		//Return false if there's an error in the form
	  		return false;
	  	}

	  	//If user is online, proceed.
	  	if(!online)
	  	{
	  		alert('You are offline!');
	  		return false;
	  	}
	}
})