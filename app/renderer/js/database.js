// Initialize the Database
var Datastore = require('nedb');

const app = require('electron').remote.app;
const path = require('path')
const userDataPath = app.getPath('userData');

userdb = new Datastore({ filename: userDataPath+'/databases/UserData.db', autoload: true });
actvusrdb = new Datastore({ filename: userDataPath+'/databases/ActiveUser.db', autoload: true });
// coopdb = new Datastore({ filename: 'databases/CoopData.db', autoload: true });
farmerdb = new Datastore({ filename: userDataPath+'/databases/FarmerData.db', autoload: true });

/*********************************************************************************
 *
 *      USER DATABASE FUNCTIONS (DESIGNED BY GLENWELL LIMITED - www.glenwell.com)
 *
 *********************************************************************************/

//Add user data
exports.addUser = function(user)
{
  //Save user to database
  userdb.insert(user, function(err, newDoc)
  {
    // Do nothing
  });
};

//Add active user data
exports.addActiveUser = function(user)
{
  //Save user to database
  actvusrdb.insert(user, function(err, newDoc)
  {
    // Do nothing
  });
};

//Login User
exports.loginUser = function(email,u_password,fnc)
{
  //Get all farmers from the DB
  userdb.findOne({ $and: [{ u_email: email }, { u_password_hash: u_password }] }, function(err, docs)
  {
    fnc(docs);
  });
}

//Find User in Database
exports.findUser = function(userData,fnc)
{
  //Get all farmers from the DB
  userdb.findOne({ $and: [{ u_email: userData.u_email }, { u_id: userData.u_id }] }, function(err, docs)
  {
    fnc(docs);
  });
}

// Returns active user only
exports.getActiveUser = function(fnc)
{
  // Get all users from the database
  actvusrdb.findOne({}, function(err, docs)
  {
    //console.log(docs);
    // Execute the parameter function
    fnc(docs);
  });
}

//Empty active user table
exports.delActiveUser = function()
{

  actvusrdb.remove({ }, { multi: true }, function(err, numRemoved)
  {
    // Do nothing
  });
}

// Deletes a user
exports.deleteUser = function(id)
{

  userdb.remove({ _id: id }, {}, function(err, numRemoved)
  {
    // Do nothing
  });
}

/**********************************
 *                                *
 *   LISTS DATABASE FUNCTIONS    *
 *                                *    
 **********************************/

// // Checks if Coop DB has data
// exports.checkCoops = function(fnc)
// {
//   coopdb.find({}, function(err, docs)
//   {
//     // Execute the parameter function
//     fnc(docs);
//   });
// }

// //Add Coop data if none exists
// exports.addCoops = function(coops)
// {
//   //console.log(docs);
//   coopdb.insert(coops, function(err, newDoc)
//   {
//     console.log(newDoc)
//   });
// }

// //Get name of Coop
// exports.coopName = function(farmer_coop, fnc)
// {
//   //Get all farmers from the DB
//   coopdb.findOne({coop_id: farmer_coop}, function(err, docs)
//   {
//     fnc(docs);
//   });
// }


/**********************************
 *                                *
 *   FARMER DATABASE FUNCTIONS    *
 *                                *    
 **********************************/

 //Add farmer data
exports.addFarmer = function(farmer_array)
{
  // Create the farmer object
  var farmer = farmer_array;

  //Save user to database
  farmerdb.insert(farmer, function(err, newDoc)
  {
    //console.log(newDoc)
    // Do nothing
  });
};

//Display all farmers under logged in user id (Field Officer View)
exports.getFarmers = function(user_id,fnc)
{
  //Get all farmers from the DB
  farmerdb.find({farmer_user_id: user_id}, function(err, docs)
  {
    // Execute the parameter function
    //console.log(docs);
    fnc(docs);
  });
}

//Display all farmers (Admin View)
exports.getAllowedFarmers = function(activeUser,fnc)
{
  if(activeUser.u_role_global==1)
  {
    //Get all farmers from the DB for admin
    farmerdb.find({}, function(err, docs)
    {
      fnc(docs);
    });
  }
  else if(activeUser.u_role_global==2)
  {
    //Get all farmers from the DB for partner
    farmerdb.find({farmer_org_id: activeUser.u_org}, function(err, docs)
    {
      // Execute the parameter function
      //console.log(docs);
      fnc(docs);
    });
  }
  else if(activeUser.u_role_global==3)
  {
    //Get all farmers from the DB for field officer
    farmerdb.find({farmer_user_id: activeUser.u_id}, function(err, docs)
    {
      // Execute the parameter function
      //console.log(docs);
      fnc(docs);
    });
  }
  else if(activeUser.u_role_global==4)
  {
    //Get all farmers from the DB for field supervisor
    farmerdb.find({farmer_coop: activeUser.u_coop}, function(err, docs)
    {
      // Execute the parameter function
      //console.log(docs);
      fnc(docs);
    });
  }
  
}


//Prepare edited farmer records for upload
exports.uploadFarmers = function(user_id,fnc)
{
  //Get all farmers from the DB that belong to the active user and have an upload status of 0 or their records have been edited before
  farmerdb.find({ $and : [{ farmer_user_id: user_id },{ $or : [ { upload_status: '0' }, { farmer_edited: '1' } ] }] }, function(err, docs)
  {
    // Execute the parameter function
    //console.log(docs);
    fnc(docs);
  });
}

//Count all farmers in the logged in user id
exports.countFarmers = function(user_id,fnc)
{
  //Get all farmers from the DB
  farmerdb.count({farmer_user_id: user_id}, function (err, count)
  {
    // Execute the parameter function
    fnc(count);
  });
}

//Count all farmers that had not been uploaded by the user
exports.notUploadedFarmers = function(user_id,fnc)
{
  //Get all farmers from the DB that belong to the active user and have an upload status of 0 or their records have been edited before
  farmerdb.count({ $and : [{ farmer_user_id: user_id },{ $or : [ { upload_status: '0' }, { farmer_edited: '1' } ] }] }, function (err, count)
  {
    // Execute the parameter function
    fnc(count);
  });
}

//Get result of a single farmer
exports.getSingleFarmer = function(farmerCode,farmerCoop,fnc)
{
  //Get all farmers from the DB
  farmerdb.findOne({ $and: [{ farmer_coop: farmerCoop }, { farmer_code: farmerCode }] }, function(err, docs)
  {
    fnc(docs);
  });
}

//Get result of a single farmer based on ID
exports.getSingleFarmerId = function(farmerId,fnc)
{
  //Get all farmers from the DB
  farmerdb.findOne({_id: farmerId}, function(err, docs)
  {
    fnc(docs);
  });
}

//Get result of a single farmer
exports.addUniqueFarmer = function(bulkFarmer)
{
  //Get all farmers from the DB
  farmerdb.findOne({ $and: [{ farmer_coop: bulkFarmer.farmer_coop }, { farmer_code: bulkFarmer.farmer_code }, { farmer_proj_area: bulkFarmer.farmer_proj_area }] }, function(err, docs)
  {
    if(docs!=null)
    {
      //console.log(docs);
    }
    else
    {
      farmerdb.insert(bulkFarmer, function(err, newDoc)
      {
        //console.log(newDoc)
      });
    }
  });
}

//Update farmer trainings
exports.updateFarmer = function(id,updates)
{
  farmerdb.update({ _id: id}, { $set: updates }, {}, function (err, numReplaced) {
});
}

//Update farmer based on server response
exports.serverUpdates = function(data)
{
  var elements = {
    "upload_status": data.upload_status,
    "farmer_promoter_id": data.farmer_promoter_id
  };

  var id = data._id;

  farmerdb.update({ _id: id}, { $set: elements }, {}, function (err, numReplaced) {
});
}
