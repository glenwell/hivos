//Script to login user
$(document).ready(function()
{
	const sha256 = require('js-sha256').sha256;
	//Variable to hold the result of the login request
	var resultLogin;
	var path= 'https://hivos.glenwell.com/admin-portal/mobile-processes';
	var options = { 
			url: path,
			beforeSubmit:  beforeLogin,  // pre-submit callback 
			success:       afterLogin,  // post-submit callback
			/*error: noWeb,*/
			cache : false,
			resetForm: true        // reset the form after successful submit 
		}; 
		
	$('#loginform').submit(function()
	{ 
		$(this).ajaxSubmit(options);  			
		// always return false to prevent standard browser submit and page navigation 
		return false; 
	}); 

	// function noWeb()
	// {
	// 	alert('No Internet Connection');
	// }
	
	//Function after login response received
	function afterLogin(resultLogin)
	{
		if(resultLogin=='not_approved')
		{
			alert('Your Account Has Not Been Approved. Contact Admin');
			return false;
		}
		else if(resultLogin=='wrong_password_username')
		{
			alert('Your username or password is wrong.');
			return false;
		}
		else if(resultLogin=='not_registered')
		{
			alert('You are not registered on our system.');
			return false;
		}
		else
		{
			//Check if returned data is of JSON type
			response = jQuery.parseJSON(resultLogin);
			if(typeof response =='object')
			{
				//Split JSON data to its individual elements
				var userData = JSON.parse(resultLogin);

				database.findUser(userData,function(existingData)
				{
					//If user exists, login without adding him to user DB...
					if(existingData!=null)
					{
						//Add his as the active user then proceed to the main page
						database.addActiveUser(userData);
					  	//Login if credentials valid
					  	window.location = "app-main.html";
					}
					else
					{
						//If the user does not exist, add him to the database then add him as active user
						database.addUser(userData);

						database.addActiveUser(userData);

						//Redirect to the main page
						window.location = "app-main.html";
					}
		  		});
				return false;
			}
			else
			{
				alert('Could Not Connect To Server.');
			}
		}
	}

	//before login
	function beforeLogin()
	{
		var online = navigator.onLine;

	  	var myresult = $('#loginform').parsley().validate();
	  	if(!myresult)
	  	{
	  		//Return false if there's an error in the form
	  		return false;
	  	}

	  	//If user is online, proceed.
	  	if(online)
	  	{
	  		
	  	}
	  	else
	  	{
	  		//Get user email and password for checking offline
		  	var u_email = $('#loginmail').val();
		  	var u_password = $('#loginpass').val();
		  	var hashed_pass= sha256(u_password);

		  	database.loginUser(u_email,hashed_pass,function(userData)
			{
				//If result from DB is not empty...
				if(userData!=null)
				{
					//Add the user as active
					database.addActiveUser(userData);
				  	//Login if credentials valid
				  	window.location = "app-main.html";
				}
				else
				{
					alert('Wrong Email/Password Combination or your records do not exist locally. Connect to the Internet to login');
					window.location = window.location.href;
				}
	  		});
	  		return false;
	  	}
	}
})